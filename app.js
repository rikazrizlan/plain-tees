const productsDOM = document.querySelector('.products-center');

//getting the products
class Products {
    async getProducts() {
        try {
            let results = await fetch("products.json");
            let data = await results.json();
            
            let products = data.items;

            //use map method to get the attributes from the items array
            products = products.map(item => {
                const {title, price} = item.fields;
                const {id} = item.sys;
                const image = item.fields.image.fields.file.url;
                return {title, price, id, image};
            })
            return products;

        } catch (error) {
            console.log(error);
        }
    }
}

//display products
class UI {
    displayProducts(products) {
        let result = '';
        products.forEach(product => {
            result += `
            <article class="product">
            <div class="img-container">
                <img src=${product.image} alt="product" class="product-img">
                <button class="bag-btn">
                    <i class="fas fa-shopping-cart"></i>
                </button>
                <button class="star-btn">
                    <i class="fas fa-star"></i>
                </button>
            </div>
            <h3>${product.title}</h3>
            <h4>LKR ${product.price}</h4>
            </article>`;
        });
        productsDOM.innerHTML = result;
    }
}


//start the functionalities when the DOM contents are loaded
document.addEventListener("DOMContentLoaded", () => {
    const ui = new UI();
    const products = new Products();

    //get all products
    products.getProducts().then(products => {
        //display products in the DOM
        ui.displayProducts(products);
    })
})
